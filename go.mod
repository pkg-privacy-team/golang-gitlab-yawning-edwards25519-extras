module gitlab.com/yawning/edwards25519-extra.git

go 1.16

require (
	filippo.io/edwards25519 v1.0.0-rc.1.0.20210721174708-390f27c3be20
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97
)
